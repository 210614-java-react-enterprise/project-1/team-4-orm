# Team 4 Project 1 - Custom Object Relational Mapping Framework

A custom object relational mapping (ORM) framework. This framework will allow for a simplified and SQL-free interaction with the relational data source.

## Tech Stack
- [ ] Java 8
- [ ] JUnit
- [ ] Mockito
- [ ] Apache Maven
- [ ] Jackson library (for JSON marshalling/unmarshalling)
- [ ] Java EE Servlet API (v4.0+)
- [ ] PostGreSQL deployed on AWS RDS
- [ ] AWS CodeBuild
- [ ] AWS CodePipeline
- [ ] Git SCM (on GitLab)

# Team Members

Curtis Greene-Morgan

Melanie Duah

Apurv Patel
